package br.senai.sp.informatica.empresaweb.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.empresaweb.model.Funcionario;

public class FuncionarioDao {

	// atributos
	Connection connection;

	// construtor
	public FuncionarioDao() {
		// abrindo conex�o com o bd
		this.connection = new ConnectionFactory().getConnection();

	}

	// m�todo salva
	public void salva(Funcionario funcionario) {

		String sql = null;
		
		if (funcionario.getId()!= null) {
			sql = "UPDATE funcionario SET nome = ?, email = ?, senha = ?, cpf = ? WHERE id = ?";
		} else {
			sql = "INSERT INTO funcionario" + 
					"(nome,email,senha, cpf) VALUES (?,?,?,?)";
		}
		
		// comando sql
		

		try {

			PreparedStatement stmt = connection.prepareStatement(sql);

			stmt.setString(1, funcionario.getNome());
			stmt.setString(2, funcionario.getEmail());
			stmt.setString(3, funcionario.getSenha());
			stmt.setString(4, funcionario.getCpf());
			
			if(funcionario.getId()!=null) {
				stmt.setLong(5, funcionario.getId());
			}
			
			stmt.execute();
			stmt.close();
			
		} catch (SQLException e) {
			throw new RuntimeException (e);
		}finally {
			
		try {
			connection.close();
		}catch (SQLException e ) {
			throw new RuntimeException(e);
			}
		
		}
	}	
	
	public void excluir(Funcionario funcionario) {
		try {
			PreparedStatement stmt = connection.prepareStatement("DELETE FROM funcionario WHERE id = ?");
			stmt.setLong(1, funcionario.getId());
			stmt.execute();
			stmt.close();
		}
			catch (SQLException e) {
				throw new RuntimeException(e);
			}finally {
				try {
					connection.close();
				}catch (SQLException e ) {
					throw new RuntimeException(e);
				}
			}
		}
	
	
		public List<Funcionario> getLista(){
			
			try {
				List<Funcionario> funcionarios = new ArrayList<>();
				
				PreparedStatement stmt = connection.prepareStatement("SELECT * FROM funcionario");
				
				ResultSet rs = stmt.executeQuery();
				
				while (rs.next()) {
					Funcionario funcionario = new Funcionario();
					funcionario.setId(rs.getLong("id"));
					funcionario.setNome(rs.getString("nome"));
					funcionario.setEmail(rs.getString("email"));
					funcionario.setSenha(rs.getString("senha"));
					funcionario.setCpf(rs.getString("cpf"));
					
					
					funcionarios.add(funcionario);
				}
				rs.close();
				stmt.close();
				
				return funcionarios;
				
			} catch (SQLException e) {
				throw new RuntimeException (e);
			}
				finally {
				try {
					connection.close();
				} catch (SQLException e) {
					throw new RuntimeException(e);
				}
			}
			
		}

	}



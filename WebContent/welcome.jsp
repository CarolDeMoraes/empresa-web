<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="prefixo" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="css/estilo-welcome.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Bem-vindo(a)</title>
</head>
<body>
<div class="tudo">
<div class="landing">	
<prefixo:import url="cabecalho.jsp"/>



<h1 style="color:white;">Bem-vindo(a) ao sistema da empresa!</h1>

<a href="#cards">
	<p class="parag btn btn-info btn-lg" >clique aqui</p>
</a>
</div>

<section id="cards">
	<h2 class="opcao">Escolha uma opção</h2>
<div class="container-fluid text-center ">
	<div class="card-group"style="width: 80%;  margin-left: auto;
    margin-right: auto;">
		<div class="card" style="width: 20rem; height: 30rem">
		<img class="card-img-top" src="imagem/contrato.jpeg" style="height: 70%" alt="Card image cap width">
		<div class="card-body">
			<h4 class="card-title">Cadastre funcionários</h4>
			<p>Clique no botão abaixo se quiser cadastrar um funcionário.</p>
			<a href="mvc?logica=AdicionaFuncionarioLogica" class="btn btn-info">Cadastre um funcionário</a>
		</div>
		</div>

		<div class="card" style="width: 20rem; height: 30rem">
		<img class="card-img-top" src="imagem/lista.jpg" style="height: 70%"alt="Card image cap">
		<div class="card-body">
			<h4 class="card-title">Liste os funcionários</h4>
			<p>Clique no botão abaixo se quiser listar os funcionários existentes na empresa.</p>
			<a href="mvc?logica=ListaFuncionariosLogica" class="btn btn-info">Lista de funcionários</a>
		</div>
		</div>
	</div>
</div>
</div>
<prefixo:import url="/WEB-INF/jsp/rodape.jsp;"/>	
</div>
</body>
</html>
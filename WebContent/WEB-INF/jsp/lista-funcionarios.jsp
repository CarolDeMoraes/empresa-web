<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%--cabeçalho da taglib core --%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="prefixo" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
 <link rel="stylesheet" type="text/css" href="estilo-lf.css">
 <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8-">
<title>Lista de funcionários</title>
<style>
	
	
	
	.link{
	color:red;
	transition: color .4s;
	font-size: 20px;
	text-decoration:none;
	}
	
	.atual{
	padding-left: 15px;
	color:#36d148;
	transition: color .4s;
	}
	
	.link:hover{
	color:#1962d1;
	text-decoration:none;	}
	

</style>
</head>
<body>
<%--importando página de fora- no caso o cabeçalho --%>
	<prefixo:import url="cabecalho.jsp"></prefixo:import>



	<%--cria uma instância de ContatoDao() --%>
	<jsp:useBean id="dao" class="br.senai.sp.informatica.empresaweb.dao.FuncionarioDao"/>
		
		
		
	<table class="table" border="1" style="border-collapse: collapse; ">
	
		<tr>
			<th>Nº</th>
			<th>Nome</th>
			<th>E-mail</th>
			<th>Cpf</th>
			<th colspan="2">Opções</th>
		</tr>		
	
	
		<prefixo:forEach var="funcionario" items="${dao.lista}" varStatus="id">
				<tr bgcolor= "#${id.count%2 == 0 ? '29c0db' : 'fffffffffffffffffff'}">
					<td>${id.count}</td>
					<td>${funcionario.nome }</td>
						<td>
							<prefixo:if test= "${not empty funcionario.email }">
							<a href="mailto:${funcionario.email}">
								${funcionario.email }
							</a>
							</prefixo:if>
							
							<prefixo:if test= "${empty funcionario.email }">
							E-mail não informado
							</prefixo:if>
						</td>
					<td>${funcionario.cpf}</td>	
					<td>
						<a class="link" href="mvc?logica=RemoveFuncionarioLogica&id=${funcionario.id}">
							✖
						</a>
						<a class="link atual" href="mvc?logica=ExibeFuncionarioLogica&id=${funcionario.id}">
							⟳
						</a>
					</td>
				</tr>
		</prefixo:forEach>
	</table>	
	<prefixo:import url="../../rodape.jsp"></prefixo:import>
</body>
</html>